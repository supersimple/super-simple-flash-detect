//**VB SCRIPT FUNCTION USED FOR IE***************************************************************//

Function GetVersionIE(i)
  on error resume next
  Dim swControl, swVersion
  swVersion = 0
  
  set swControl = CreateObject("ShockwaveFlash.ShockwaveFlash." + CStr(i))
  if (IsObject(swControl)) then
    swVersion = swControl.GetVariable("$version")
  end if
  GetVersionIE = swVersion
End Function

//**********************************************************************************************//
