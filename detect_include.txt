//**CUT AND PASTE CODE BELOW INTO YOUR PAGE(S)**********************************//


<script type="text/javascript">
	/**************************************************/
	// Super Simple Detect Script v3
	//
	// Available at supersimple.org
	// Authored by: Todd Resudek - tr1design.net
	//				Brian Kiel - briankiel.com
	//
	/**************************************************/
	
	// To override constants on a specific page, set the vars here
	//** make sure to remove the comment tags ("//") from the line that you are adjusting :)
	
	//expireHour = ;
	//versionNeeded = ;
	//directToFail = '';
	//directToPass = '';
	//alertOnFail = ;
	//useCookies = ;
	
</script>	
<script type="text/javascript" src="detect_vars.js" ></script>
<script language="VBScript" type="text/vbscript" src="detect_vb.js"></script>
<script type="text/javascript" src="detect_js.js" ></script>