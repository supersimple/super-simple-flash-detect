//*****SCRIPT DETAILS***********************************************************************/

Super Simple Flash Detect Script
Released: Nov 2005
Revised: Aug 2006 (version 3)
		 		 Jan 2009 (versIon 3_3) - Adds support for versions 10+
		 		 Oct 2011 (version 3_4) - Updates link to download Adobe Flash Player
		 		 
Author(s): Todd Resudek - tr1design.net
		   Brian Kiel - briankiel.com

Notes:
This code is open-source, free to use privately or commercially.

This script will detect which Flash version a visitor has installed so that you 
can alert the user to upgrade, if need-be, before visiting pages on your site.

There is an option to cookie the user, so that the detection script will only run
once. This is useful for sites with several or more pages, in order to prevent 
the user from undergoing flash detection on every page. The script will first look
for the cookie, and only if it does not exist, will it execute the detection portion
of the script.

Code is for modern browsers and tested on:
safari, firefox, mozilla, netscape 7, gecko, opera 7+, mac ie5.2, win ie6

This script was written for supersimple.org and TEQUILA.com
Based on a detection script written by Colin Moock. (<-- the man! )
Update based on Adobe� Flash player detection

//*****VARIABLES****************************************************************************/


There is little to do to install this script on your site. Just set the 6 
variables in the "GLOBALS" area in detect_vars.js and include the scripts
on your page (code in detect_include.txt). The variables are:
	
	a) cookieExpiresInHours
		- (Integer) The number of hours until the cookie expires. It is recommended to keep
		This number reasonably low so that if a user should downgraade their 
		flash plug-in at some point in the future, the cookie will have expired 
		already so that the detection will run again. 2-10 hours is a suggested 
		value. *Note that you can also set this to a negative integer, which will
		expire at the end of each session. **Cookies can also be disabled (see below)
		
	b) flashVersionNeeded
		- (Integer) The minimum flash version that you are testing for. This should be
		the minimum for the entire site. Individual pages can be set for higher versions
		if need-be. For example, if your site uses flash 6 navigation, but has 1 page of
		videos that requires flash 8, set the script to look for your baseline version (6).
		Tag the videos page to require version 8. (See 'Exceptions' below)
		
	c) directToFailPage
		- (String) The URL to direct the visitor to if they fail the test 
		(ie. dont have a high enough version). This is set, by default to the Flash 
		Player download page on Adobe's site. If you have an 'upgrade' page on your
		site, set that page as the URL. 
		
		
	d) directToPassPage
		- (String) The URL to direct the visitor to if they pass the test 
		(ie. have a version greater then or equal to the required version).
		Leave this empty ('') in order to stay on the page that the user requested.
		One possible reason that you would want to set this URL would be that you 
		have a flash version, and an HTML version of a site, and you want
		all users that have flash to see the flash version. You can also call a
		function upon passing. Use 'fcn: functionName()' format to do so. For
		example, directToPassPage = 'fcn: writeSomething()';
		
	e) showAlertOnFail
		- (Boolean) This sets the option of whether or not the user should see 
		an alert box before redirecting. This alert says "This page requires Flash 
		Version n. Please upgrade". If you just want to redirect the user, set this
		to false.
		
	f) enableCookies
		- (Boolean) This sets the option of whether or not to set cookies after
		the test. Generally, you will want to set this to true to save the user
		from repeatedly undergoing the test. *Note that user that have cookies 
		turned off, will simply undergo the test each time. There will be no errors.



//*****EXCEPTIONS***************************************************************************/


There will be times when you want individual pages to be treated differently.  For example, 
if your site uses flash 6 navigation, but has 1 page of videos that requires flash 8. In this
case, you can set the script on that videos page to look for flash 8, and the rest of the
site to only require flash 6. 

To override a setting from detect_vars.js, set the corresponding variable in the page
that you want to be effected.


//*****INCLUDING***************************************************************************/


Include the scripts into any page that you want to detect flash. The includes should be 
cut-and-pasted from the detect_include.txt file included in this package. 

It is imperative that the scripts be included in the following order:

1) any exceptions to the vars set in detect_vars.js
2) detect_vars.js
3) detect_vb.js
4) detect_js.js

Missing any of the files, or including them in improper order will cause errors.






