//**CHECK FOR ANY OVERIDDEN CONSTANTS***********************************************************//

if(!expireHour){var expireHour = cookieExpiresInHours;}
if(!versionNeeded){var versionNeeded = flashVersionNeeded;}
if(!directToFail){var directToFail = directToFailPage;}
if(!directToPass){var directToPass = directToPassPage;}
if(!alertOnFail){var alertOnFail = showAlertOnFail;}
if(!useCookies){var useCookies = enableCookies;}


//**********************************************************************************************//


//**JS SCRIPT FUNCTION USED FOR NON-IE BROWSERS*************************************************//

function GetVersionNonIE(){

	if(window.navigator.plugins){
		var PI = window.navigator.plugins;
			for(var i =0; i < PI.length; i++){
				if(window.navigator.plugins[i].name.indexOf('Flash') != -1){
				//character at 16 is the flash version number
				var vers = window.navigator.plugins[i].description.charAt(16);
				//check for a 2 digit major version
				if(vers == '1'){ vers += window.navigator.plugins[i].description.charAt(17);}
 				}
	}
	//return the flash version
	return vers;
	}else {
	//the browser cannot understand the request for plug-in info
	return false;
	}
 
}

//**********************************************************************************************//

//**DETECT FOR IE BROWSE************************************************************************//

// Detect WIN/IE Configuration and throw VB at it to get the flash version
if(navigator.appVersion.indexOf("MSIE") != -1){var IE = true;}else{ var IE = false;}
if(navigator.appVersion.toLowerCase().indexOf("win") != -1){var WIN = true;}else{ var WIN = false;}
if(navigator.userAgent.indexOf("Opera") != -1){var OP = true;}else{var OP = false;}


//**********************************************************************************************//

//**DETECT FOR IE BROWSER***********************************************************************//

function getFlashVersion(){

	// Write vbscript detection on ie win. IE on Windows doesn't support regular
	// JavaScript plugins array detection.
	if(IE && WIN && !OP){
		for(var i=20; i>0; i--){
			var flasharray = GetVersionIE(i);
		}
		//VB returns an array of the plug-in info, so we will extract the major version
		var flashinfo = flasharray.split(" ");
		var versioninfo = flashinfo[1];
		var versionarray = versioninfo.split(",");
	
		//this is the major version
		var flashVersion = versionarray[0];
		
	}else{
		var flashVersion = GetVersionNonIE();
	}
	
	return flashVersion;

}
//************************************************************************************************//


//*FUNCTION FOR SETTING COOKIE********************************************************************//

function set_detect_cookie(userVer){
	
	expireDate = new Date;
	expireDate.setHours(expireDate.getHours()+(expireHour));
	
	var flashPair = 'flashVer='+userVer+';expires='+expireDate.toGMTString();
	document.cookie = flashPair;

}

//************************************************************************************************//


//*FUNCTION FOR CLEARING COOKIE********************************************************************//

//function for clearing cookie so that users can download the new flash player and try again.
function clear_detect_cookie(cookieName){
	var thisCookie = document.cookie.split('; ');
	expireDate = new Date;
	expireDate.setHours(expireDate.getDate()-1);
	
	for(i=0; i<thisCookie.length; i++){
		if(cookieName == thisCookie[i].split('=')[0]){
			thisCookie[i] = 'flashVer='+userVer+';expires='+expireDate.toGMTString();
		}
	}	
}

//************************************************************************************************//


//*FUNCTION FOR GETTING COOKIE********************************************************************//

function get_detect_cookie_value(cookieName){
	var thiscookie = document.cookie.split('; ');
	for(i=0; i<thiscookie.length; i++){
		if(cookieName == thiscookie[i].split('=')[0]){
			return thiscookie[i].split('=')[1];
		}
	}
	//if we got no matches
	return false
}

//************************************************************************************************//


//*FUNCTION TO ENVOKE DETECT**********************************************************************//

function beginFlashDetection(){

if(document.cookie.indexOf('flashVer') != -1){
	//user has cookie already, lets get the version
	var flashVersion = get_detect_cookie_value('flashVer');
}else{
	// Version check based upon the values entered above in "Globals"
	if(versionNeeded <= getFlashVersion()){
		var hasReqVer = true;
	}else{
		var hasReqVer = false;
	}
}

// Check to see if the version meets the requirements for playback
if (hasReqVer || flashVersion >= versionNeeded) {  // if we've detected an acceptable version
	
	//set a cookie with version
	if(!flashVersion){
		//set cookie if option is enabled
		if(useCookies){set_detect_cookie(getFlashVersion());}
	}
  	//if there is a pass page, direct to that. Also, look for a function to call instead of redirecting
  	if(directToPass != ''){
  		//test for function
  		if(directToPass.substring(0,4) == 'fcn:'){
  			//it is a function call
  			eval(directToPass.substring(5, directToPass.length));
  		}else{
  			//it is a page
  			window.location = directToPass;
  		}
  	}
  
} else {  // flash is too old or we can't detect the plugin
	//clear the cookie that we set so that the user can download the lfash player and try again
	if(useCookies){clear_detect_cookie('flashVer');}
	//show js alert, if set to true
	if(alertOnFail){alert('This page requires Flash Plug-In version '+ versionNeeded +'. Please upgrade.');}
	//redirect to our non-flash page. If set to '', direct the user to Adobe download page
	if(directToFail == ''){window.location = 'http://get.adobe.com/flashplayer/';}else{window.location = directToFail;}
}


}



