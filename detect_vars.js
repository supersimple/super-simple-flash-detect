//*****RELEASE NOTES**************************************************************/
//
//	Super Simple Flash Detect Script
//  Released: Nov 2005
//  Revised: Aug 2006 (version 3)
//	Revised: Oct 2011 (version 3.4)
//	
//	This product is open-source, so feel free to use it on any projects,
//  private or commercial. Full source available at: http://www.supersimple.org
//  Please leave this entire message intact.
//	
//	Written by:
//	Todd Resudek - tr1design.net
//	& Brian Kiel - briankiel.com
//	for SuperSimple.org. 
//  Based on a detection script written by Colin Moock. (<-- the man! )
//	Update based on Adobe� Flash player detection
//
//*********************************************************************************/

window.onload=function(){beginFlashDetection();}

//**GLOBALS****************************************************************************************/
 
cookieExpiresInHours = 2; //the number of hours until the cookie expires. set to a negative integer to make the cookie expire immediately
flashVersionNeeded = 9; //set min. flash player major version need for this site
directToFailPage = ''; //set the URL to go to if they fail the test, if left blank, it will redirect to Adobe's download page
directToPassPage = ''; //leave this empty to remain on this page after test. Use 'fcn: functionName()' to call a function on pass.
showAlertOnFail = true; //this will pop-up an alert box warning the user that they do not have the required flash version before redirecting
enableCookies = true; //this determines whether a cookie is set or not

//*************************************************************************************************//